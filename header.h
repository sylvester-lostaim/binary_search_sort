#ifndef HEADER_H
#define HEADER_H
#include<string>
#include<fstream>
using namespace std;
class proj
{
	struct info
	{
		char curr[10];
	};
	info curr[4];//0=add,1=delete,2=update,3=recover data
	char input[3],dir[41],*dir2,x[11],*cmp1,*cmp2,build_cond;
	int input2,data,count,z;
	float y;
	string filename,show;
	struct ret
	{
		public:
			char code[10];
			int stock;
			float price;
			float subtotal;
	};
	class rec
	{
		public:
			ret a;
			rec *xxx1,*xxx2,*aim;
	};
	rec *first,*last,*now,*find1,*find2,*find3;
	ret backup[5];
public:
	void login();						//login surface
	proj();
	//function that related to
	void title();						//all interfaces
	void checkinput(int,int);			//bridges working interfaces
	void file_exist(char);
	bool searching(char[10],int,int);	//inside building interfaces(working data)
	void add_data();
	bool add_file(ifstream&,string);
	bool add_file_fail();
	void delete_all();
	void store_undo();
	void arranging(int,int);
	void arr_num(char);
	bool arr_num2(char);
	void display_order(char);
	bool checkinput2(char);
	void write_file(char);
	bool ask();							// ''     ''         ''     (confirmation)
	//independent functions
	void home();						//1st interface
	void openfile();					//2nd interface
	void newfile();
	void file();						//3rd interface
	void building();					//4th interface
	void finding();
	void arrange();
	void compare();
	void add();							//5th interface
	void update();
	void del();
	void del_history();
	void saving();
};
#endif