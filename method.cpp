#include <iostream>
#include "header.h"
#include<string>
#include<cstring>
#include<cstdlib>
#include<fstream>
#include<iomanip>
using namespace std;
proj::proj()
{
	count=data=0;
	for(int i=0;i<4;i++)
		strcpy(curr[i].curr,"");
	build_cond='P';
}
void proj::login()
{
	for(;;)
	{
		system("cls");
		cout<<"\n(LOGIN)\nUnauthorised access is strictly not allowed!\n\nID       : ";
		cin>>filename;
		cout<<"Password : ";
		cin>>show;
		if(filename!="1"||show!="1")
		{
			cout<<"\nWrong ID or password.\n";
			system("pause");
		}
		else
		{
			filename.clear();
			show.clear();
			cin.ignore(1);
			home();
		}
	}
}
void proj::title()
{
	system("cls");
	cout<<"\t\t  Supermarket Warehouse Stock Recording System\n\nFilename : "<<filename<<endl<<"Location : "<<dir;
}
void proj::checkinput(int a,int b)
{
	for(;;)
	{
	cout<<"\n\nInput : ";
	cin.get(input,3);
	cin.clear();
	cin.ignore(9999999,'\n');
	input2=atoi(input);
	if(strlen(input)>1||(input2>b||input2<a))
		cout<<"(Input range: "<<a<<" - "<<b<<")";
	else
		break;
	}
}
bool proj::checkinput2(char a)
{
	for(;;)
	{
		cout<<"Product Code : ";
		cin.get(x,11);
		cin.clear();
		cin.ignore(9999999,'\n');
		if(x[0]=='?'&&strlen(x)==1)
			return 1;
		if(a=='U'&&strcmp(x,"x")==0)
		{
			strcpy(x,find2->a.code);
			break;
		}
		if(strlen(x)!=9||isdigit(x[0])||isdigit(x[1])||isdigit(x[2])||isalpha(x[3])||isalpha(x[4])||isalpha(x[5])||isalpha(x[6])||isalpha(x[7])||isdigit(x[8]))
			cout<<"(Format : XXX00000Y \; Eg : ABC12345A)\n\n";
		else if(isspace(x[0])||isspace(x[8]))
			cout<<"(No <space> at both ends.)\n\n";
		else
			break;
	}
	if(a=='0'||a=='U')
	{
		for(;;)
		{
			cout<<"Unit Price   : ";
			getline(cin,show,'\n');
			if(show=="?"&&show.length()==1)
				return 1;
			if(a=='U'&&show=="x"&&show.length()==1)
			{
				y=find2->a.price;
				break;
			}
			input2=0;
			size_t found=show.find_first_of(".");
			size_t found2=show.find_last_of(".");
			if(found!=found2)
			{
				cout<<"<Decimal number with one decimal mark only>\n\n";
				show="";
				input2=1;
			}
			if(input2==0)
			{
				size_t found=show.find_first_not_of("1234567890.");
				if(found!=std::string::npos)
				{
					cout<<"<Decimal number with one decimal mark only>\n\n";
					show="";
					input2=1;					
				}
			}
			if(input2==0)
			{
				y=stof(show);
				break;
			}
		}
		for(;;)
		{
			cout<<"Quantity     : ";
			getline(cin,show,'\n');
			if(show=="?"&&show.length()==1)
				return 1;
			if(a=='U'&&show=="x"&&show.length()==1)
			{
				z=find2->a.stock;
				break;
			}
			input2=0;
			size_t found=show.find_first_not_of("1234567890");
			if(found!=std::string::npos)
			{
				cout<<"<Integer only.>\n\n";
				show="";
				input2=1;
			}
			if(input2==0)
			{
				z=stoi(show);
				break;
			}
		}
	}
	return 0;
}
bool proj::ask()
{
	for(;;)
	{
		cout<<"\n\nInput : ";
		cin.get(input,3);
		cin.clear();
		cin.ignore(9999999,'\n');
		if(strlen(input)==1&&(strcmp(input,"Y")==0||strcmp(input,"y")==0))
			return 1;
		else if(strlen(input)==1&&(strcmp(input,"N")==0||strcmp(input,"n")==0))
			return 0;
		/*
		{
				cout<<"Replace xxx with new xxx"<<"?\n";						//add, update
				cout<<"Delete xxx from the list?\n";							//delete
				cout<<"Remove xxx, then move xxx back into the list?\n";		//undo1
				cout<<"Move the previous xxx back into the list?\n";			//undo2
		}*/
	}
}
void proj::file_exist(char a)
{
	cout<<"\n\n(Type '?' to return to previous page.)\n\n";
	for(;;)
	{
		for(;;)
		{
			if(a=='1')
				cout<<"New ";
			cout<<"File Name : ";
			getline(cin,filename);
			if(filename=="?"&&filename.length()==1)
			{
				if(a=='1')
					dir2=strstr(dir," > New");
				else
					dir2=strstr(dir," > Open");
				strcpy(dir2,"");
				filename="-";
				home();
			}
			input2=0;
			size_t found=filename.find_first_of("\\/:?*<\">|");
			if(found!=string::npos)
			{
				cout<<"\n\t     \\ / : ? * < \" > | "<<" cannot be containing in file name.\n\n";
				filename="-";
				input2=1;
			}
			if(filename.length()==0)
			{
				cout<<"\n(Please insert a filename.)\n";
				input2=1;
			}
			if(input2==0)
				break;
		}
		filename+=".txt";
		ifstream aaa(filename.c_str());
		if(a=='1')
		{
			if(aaa.fail()==0)
			{
				cout<<"("<<filename<<" had already existed.)\n\n";
				aaa.close();
				filename="-";
			}
			else
			{
				strcat(dir," > Build");
				build_cond='F';
				building();
			}
		}
		else
		{
			if(aaa.fail()==1)
				cout<<"("<<filename<<" is not exist or maybe cannot be accessed.)\n\n";
			else if(a=='2')
			{
				if(add_file(aaa,filename))
				{
					strcat(dir," > File");
					aaa.close();
					file();
				}
				else
				{
					aaa.close();
					delete_all();
					home();
				}
			}
		}
	}
}
void proj::add_data()
{
	rec *New=new rec;
	if(data!=0)
	{
		if(searching(x,0,9))
		{
			cout<<"\n\n| Similar product code is not allowed. |\n| "<<x<<" had been recorded as below.|\n\n";
			cout<<"Product Code : "<<find2->a.code<<"\nUnit Price   : "<<setprecision(3)<<find2->a.price<<"\nQuantity     : "<<setprecision(0)<<find2->a.stock<<"\n\n\n\nDeleting newest similar data . . .\n";
			system("pause");
		}
	}
	strcpy_s(New->a.code,x);
	strcpy(curr[0].curr,x);
	New->a.price=y;
	New->a.stock=z;
	New->a.subtotal=y*z;
	if(data==0)
	{
		last=first=New;
		first->xxx1=last->xxx2=first;
	}
	else
	{
		last=New;
		last->xxx2=now;
		last->xxx1=first;
		first->xxx2=last;
		now->xxx1=last;
	}
	now=last;
	data++;
}
bool proj::add_file(ifstream &a,string b)
{
	float c=0;
	string *bb;
	while(b.length()!=1)
	{
		a>>b;
		c++;
		if(c>30)
		{
			cout<<"No data can be found in file within expected range.\n";
			system("pause");
			home();
		}
	}
	if(c<31)
	{
		size_t bb=b.find_first_not_of("1234567890");
		if(bb!=b.npos)
			cout<<"The data cannot be read without following the format.\n";
		else
		{
			char d='b';
			for(;;)
			{
				rec *New=new rec;
				if(data==0)
				{
					last=first=New;
					first->xxx1=last->xxx2=first;
				}
				else
				{
					last=New;
					last->xxx2=now;
					last->xxx1=first;
					first->xxx2=last;
					now->xxx1=last;
				}
				now=last;
				a>>New->a.code;
				if(a.fail())
				{
					a.clear();
					a.ignore(9999999,'\n');
					break;
				}
				if(strlen(New->a.code)!=9||isdigit(New->a.code[0])||isdigit(New->a.code[1])||isdigit(New->a.code[2])||isalpha(New->a.code[3])||isalpha(New->a.code[4])||isalpha(New->a.code[5])||isalpha(New->a.code[6])||isalpha(New->a.code[7])||isdigit(New->a.code[8]))
					break;
				a>>New->a.price;
				if(a.fail())
				{
					a.clear();
					a.ignore(9999999,'\n');
					break;
				}
				a>>New->a.stock;
				if(a.fail())
				{
					a.clear();
					a.ignore(9999999,'\n');
					break;
				}
				a>>c;
				if(a.fail())
				{
					a.clear();
					a.ignore(9999999,'\n');
					break;
				}
				if(New->a.price<0||New->a.stock<0||c<0)
					break;
				data++;
				New->a.subtotal=New->a.price*New->a.stock;
				a>>c;
				if(a.fail())
				{
					a.clear();
					a.ignore(9999999,'\n');
					d='a';
					break;
				}
			}
			cout<<"\nEnd of File (or maybe due to irrelevant data)\nStop reading file.\n\n";
			a.close();
			display_order('2');
			if(d=='b')
			{
				last=last->xxx2;
				cout<<"Irrelevent data had been detected at last column or the next in the list.\n\n\n\nPlease change your data into following format:\n\nLEFT\t\t\t\t\t\t\t\t      RIGHT\nNumbering <space> Product code <space> Price <space> Stock <space> subtotal\n\n\n\n(DETAIL)\nNumbering    : 1 (or any integer)\nProduct Code : ABC12345A (XXX00000X where X is character, Y is number)\nPrice        : 12.352\nStock        : 110\nSubtotal     : 1358.72\n\n[NOTE: unit price, quantity, subtotal cannot be negative / no <space> for product code.]\n\n\n\n\nYou will be redirected to home.\n";
				system("pause");
				return false;
			}
			system("pause");
		}
		return true;
	}
}
void proj::arranging(int a,int b)
{
	for(;;)
	{
		int j=0;
		find1=first;
		find2=first->xxx1;
		cmp1=&find1->a.code[b];
		cmp2=&find2->a.code[b];
		for(int i=1;i<data;i++)
		{
			if(strncmp(cmp1,cmp2,a)>0)
			{	
				if(data>2)
				{
					find3=find2->xxx1;
					find2->xxx1=find1;
					find2->xxx2=find1->xxx2;
					find3->xxx2=find1;
					find1->xxx1=find3;
					find1->xxx2=find2;
					find3=find2->xxx2;
					find3->xxx1=find2;
					find3=find1;
					find1=find2;
					find2=find3;
					j=1;
					if(i==1)
						first=find1;
				}
				else if(data==2)
				{
					first=find2;
					last=find1;
				}
			}
			find1=find2;
			find2=find2->xxx1;
			cmp1=&find1->a.code[b];
			cmp2=&find2->a.code[b];
		}
		last=first->xxx2;
		now=last;
		if(j==0)
			break;
	}
}
void proj::arr_num(char a)
{
	for(;;)
	{
		int j=0;
		find1=first;
		find2=first->xxx1;
		for(int i=1;i<data;i++)
		{
			if(arr_num2(a)==true)
			{	
				if(data>2)
				{
					find3=find2->xxx1;
					find2->xxx1=find1;
					find2->xxx2=find1->xxx2;
					find3->xxx2=find1;
					find1->xxx1=find3;
					find1->xxx2=find2;
					find3=find2->xxx2;
					find3->xxx1=find2;
					find3=find1;
					find1=find2;
					find2=find3;
					j=1;
					if(i==1)
						first=find1;
				}
				else if(data==2)
				{
					first=find2;
					last=find1;
				}
			}
			find1=find2;
			find2=find2->xxx1;
		}
		last=first->xxx2;
		now=last;
		if(j==0)
			break;
	}
}
bool proj::arr_num2(char a)
{
	switch(a)
	{
		case'1':
			if(find1->a.price>find2->a.price)
				return 1;
			else
				return 0;
		case'2':
			if(find1->a.stock>find2->a.stock)
				return 1;
			else
				return 0;
		case'3':
			if(find1->a.subtotal>find2->a.subtotal)
				return 1;
			else
				return 0;
	}
}
bool proj::searching(char a[10],int b,int bb)
{
	char *c,*d,*e;
	find2=find1=first;
	find3=last;
	int i=(data-1)/2;
	for(int j=0;j<i;j++)
		find2=find2->xxx1;
	for(;;)
	{
		c=&find2->a.code[b];
		if(strncmp(a,c,bb)<0&&i>0)
		{
			find3=find2->xxx2;
			for(int j=0;j<i;j++)
				find2=find2->xxx2;
		}
		else if(strncmp(a,c,bb)>0&&i>0)
		{
			find1=find2->xxx1;
			for(int j=0;j<i;j++)
				find2=find2->xxx1;
		}
		else 
			break;
		i/=2;
	}
	d=&find1->a.code[b];
	e=&find3->a.code[b];
	if(strncmp(a,d,bb)==0||strncmp(a,c,bb)==0||strncmp(a,e,bb)==0)
	{
		if(strncmp(a,d,bb)==0)
			find2=find1;
		else if(strncmp(a,e,bb)==0)
			find2=find3;
		return 1;
	}
	return 0;
}
void proj::display_order(char a)//1 is add, 6 is reading file, 3 is delete, 4 is updated, 5 is descend, 6 is ascend
{
	float j,l;
	int i,k;
	i=j=k=l=0;
	cout<<"\n\n\t\t\t\tITEM LIST\n------------------------------------------------------------------------------\n No."<<setw(17)<<"Product code"<<setw(17)<<"Unit Price"<<setw(17)<<"Quantity"<<"\t      Subtotal Price\n------------------------------------------------------------------------------\n";
	if(a!='5')
	{
		for(i;i<data;i++)
		{
			cout<<setw(4)<<i+1<<setw(17)<<first->a.code<<setw(17)<<fixed<<setprecision(3)<<first->a.price<<setw(17)<<setprecision(0)<<first->a.stock<<"\t      "<<setprecision(3)<<first->a.subtotal<<endl;
			j+=first->a.price;
			k+=first->a.stock;
			l+=first->a.subtotal;
			first=first->xxx1;
		}
	}
	else
	{
		for(i;i<data;i++)
		{
			first=first->xxx2;
			cout<<setw(4)<<i+1<<setw(17)<<first->a.code<<setw(17)<<first->a.price<<setw(17)<<first->a.stock<<"\t      "<<first->a.subtotal<<endl;
			j+=first->a.price;
			k+=first->a.stock;
			l+=first->a.subtotal;
		}
	}
	cout<<"\n------------------------------------------------------------------------------\nTOTAL:\n"<<setfill(' ')<<setw(11)<<i<<" item(s)"<<setfill(' ')<<setw(19)<<fixed<<setprecision(3)<<j<<setfill(' ')<<setw(17)<<setprecision(0)<<k<<"\t      "<<fixed<<setprecision(3)<<l<<"\n------------------------------------------------------------------------------\n";
	if(a<'5')
	{
		if(a!='2')
			cout<<"All data had been automatically sorted.\n\n\n\n";
		switch(a)
		{
		case'1':cout<<"Most Current Added : "<<curr[0].curr;break;
		case'3':cout<<"Most Current Deleted : "<<curr[1].curr;break;
		case'4':cout<<"Most Current Updated : "<<curr[2].curr;break;
		}
		cout<<"\n(Type '?' to return to previous page.)\n\n";
	}
}
void proj::delete_all()
{
	if(data!=0)
	{
		last->xxx1=last;
		for(data;data>0;data--)
		{
			now=first;
			first=first->xxx1;
			delete now;
		}
	}
	for(int i=0;i<4;i++)
		strcpy(curr[i].curr,"");
	for(int i=0;i<5;i++)
	{
		strcpy(backup[i].code,"");
		backup[i].price=NULL;
		backup[i].stock=NULL;
		backup[i].subtotal=NULL;
	}
	count=0;
	build_cond='P';
}
void proj::store_undo()
{
	if(count==5)
	{
		for(int i=0;i<4;i++)
		{
			strcpy(backup[i].code,backup[i+1].code);
			backup[i].price=backup[i+1].price;
			backup[i].stock=backup[i+1].stock;
			backup[i].subtotal=backup[i+1].subtotal;
		}
		strcpy(backup[4].code,find2->a.code);
		backup[4].price=find2->a.price;
		backup[4].stock=find2->a.stock;
		backup[4].subtotal=find2->a.subtotal;
	}
	else
	{
		strcpy(backup[count].code,find2->a.code);
		backup[count].price=find2->a.price;
		backup[count].stock=find2->a.stock;
		backup[count].subtotal=find2->a.subtotal;
		count++;
	}
}
void proj::write_file(char a)
{
	ofstream aaa(filename.c_str());
	aaa<<"\t\t\t\tITEM LIST\n------------------------------------------------------------------------------\n No."<<setw(17)<<"Product code"<<setw(17)<<"Unit Price"<<setw(17)<<"Quantity"<<"\t      Subtotal Price\n------------------------------------------------------------------------------\n";
	float j,l;
	int i,k;
	i=j=k=l=0;
	if(a=='1')
	{
		for(i;i<data;i++)
		{
			aaa<<setw(4)<<i+1<<setw(17)<<first->a.code<<setw(17)<<fixed<<setprecision(3)<<first->a.price<<setw(17)<<setprecision(0)<<first->a.stock<<"\t      "<<setprecision(3)<<first->a.subtotal<<endl;
			j+=first->a.price;
			k+=first->a.stock;
			l+=first->a.subtotal;
			first=first->xxx1;
		}
	}
	else
	{
		for(i;i<data;i++)
		{
			first=first->xxx2;
			aaa<<setw(4)<<i+1<<setw(17)<<first->a.code<<setw(17)<<first->a.price<<setw(17)<<first->a.stock<<"\t      "<<first->a.subtotal<<endl;
			j+=first->a.price;
			k+=first->a.stock;
			l+=first->a.subtotal;
		}
	}
	aaa<<"\n------------------------------------------------------------------------------\nTOTAL:\n"<<setfill(' ')<<setw(11)<<i<<" item(s)"<<setfill(' ')<<setw(19)<<fixed<<setprecision(3)<<j<<setfill(' ')<<setw(17)<<setprecision(0)<<k<<"\t      "<<fixed<<setprecision(3)<<l<<"\n------------------------------------------------------------------------------\n";
	aaa.close();
}
////////////////////////////////////////////////////////////////ALL INTERFACES//////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////1st interface////////////////////////////////////////////////////////////////////////////

void proj::home()
{
	strcpy(dir,"Home");
	filename="-";
	title();
	cout<<"\n\n1 - New File\n2 - Open File\n3 - Log Out\n4 - Exit";
	checkinput(1,4);
	switch(input2)
	{
		case 1:
			strcat(dir," > New");
			newfile();
			break;
		case 2:
			strcat(dir," > Open");
			openfile();
			break;
		case 3:
			login();
		case 4:
			exit(0);
	}
}
///////////////////////////////////////////////////////////////2nd interface////////////////////////////////////////////////////////////////////////////
void proj::newfile()
{
	title();
	file_exist('1');
}
void proj::openfile()
{
	title();
	file_exist('2');
}
///////////////////////////////////////////////////////////////3rd interface////////////////////////////////////////////////////////////////////////////
void proj::file()
{
	title();
	cout<<"\n\n1 - Find\n2 - Edit\n3 - Arrange\n4 - Compare\n5 - Back";
	checkinput(1,5);
	switch(input2)
	{
		case 1:
			strcat(dir," > Find");
			finding();
			break;
		case 2:
			strcat(dir," > Edit");
			building();
			break;
		case 3:
			strcat(dir," > Arrange");
			arrange();
			break;
		case 4:
			strcat(dir," > Compare");
			compare();
			break;
		case 5:
			delete_all();
			cout<<"\nClosing file . . .\n(You will be redirected to Home.)\n\n";
			system("pause");
			home();
			break;
	}
}
///////////////////////////////////////////////////////////////4th interface////////////////////////////////////////////////////////////////////////////
void proj::building()
{
	title();
	cout<<"\n\n1 - Add\n2 - Update\n3 - Delete\n4 - Recover deleted data (5 histories in maximum)\n5 - Save\n6 - Back";
	checkinput(1,6);
	switch(input2)
	{
		case 1:
			strcat(dir," > Add");
			add();
			break;
		case 2:
			strcat(dir," > Update");
			update();
			break;
		case 3:
			strcat(dir," > Delete");
			del();
			break;
		case 4:
			strcat(dir," > Data Recovery");
			del_history();
			break;
		case 5:
			strcat(dir," > Save");
			saving();
			break;
		case 6:
			if(build_cond=='P')
			{
				if(data==0)
				{
					cout<<"\nTotal data : 0\nOnly proceed with at least one data.\n\n";
					system("pause");
					building();
				}
				else
				{
					cout<<"\nIf there is any data changing, please save if the\ndata is not for temporary use.\n\nReturn? (Yes: Y/y ; No: N/n)";
					if(ask())
					{
						dir2=strstr(dir," > Edit");
						strcpy(dir2,"");
						file();
					}
					else
						building();
				}
			}
			else
			{
				if(data==0)
				{
					cout<<"\nTotal data : 0\nThe new \""<<filename<<"\" cannot not created successfully.\n\n\n\n\nYou will be redirected to home.\n";
					system("pause");
					delete_all();
					home();
				}
				else
				{
					cout<<"\nAll current data is temporary exist in this program,\nall data will be erased if proceed.\n\nReturn to HOME? (Yes: Y/y ; No: N/n)";
					if(ask())
					{
						delete_all();
						home();
					}
					else
						building();
				}
			}
	}
}
void proj::finding()
{
	int a,b;
	char *check;
	for(;;)
	{
		title();
		cout<<"\n\nNOTE:\n  (i) '=' represents a random character excluding itself.\n (ii) Insert '?' only to return.\n\n";
		do
		{
			cout<<setw(37)<<"(123456789)"<<"\nSearching <9 characters> : ";
			getline(cin,show);
			if(show=="?"&&show.length()==1)
			{
				dir2=strstr(dir," > Find");
				strcpy(dir2,"");
				file();
			}
		}
		while(show.length()!=9);
		size_t found=show.find_first_not_of("=");
		switch(found)
		{
		case 0:a=9;b=0;break;
		case 1:a=8;b=1;break;
		case 2:a=7;b=2;break;
		case 3:a=6;b=3;break;
		case 4:a=5;b=4;break;
		case 5:a=4;b=5;break;
		case 6:a=3;b=6;break;
		case 7:a=2;b=7;break;
		case 8:a=1;b=8;break;
		default:
			cout<<"\nPlease include at least one recognizable character.\n";
			system("pause");
			finding();
		}
		strcpy(x,show.c_str());
		char z[2];
		check=&x[b];
		strncpy(z,check,1);
		arranging(9,0);
		arranging(a,b);
		if(!searching(z,b,1))
		{
			cout<<"No entries found.\n";
			system("pause");
			continue;
		}
		while(strncmp(z,&find2->xxx2->a.code[b],1)==0)
		{
			find2=find2->xxx2;
			check=&find2->xxx2->a.code[b];
		}
		find3=find2;
		while(strncmp(z,&find3->xxx1->a.code[b],1)==0)
		{
			find3->aim=find3->xxx1;
			find3=find3->xxx1;
			check=&find3->xxx1->a.code[b];
		}
		find3->aim=0;
		int i=1,k=0;
		float j=0,l=0;
		for(;;)
		{
			found=show.find_first_not_of("=",found+1);
			if(i==0)
			{
				cout<<"No entries found.\n";
				break;
			}
			if(found>8)
			{
				find1=find2;
				cout<<"\n\n------------------------------------------------------------------------------\n No."<<setw(17)<<"Product code"<<setw(17)<<"Unit Price"<<setw(17)<<"Quantity"<<"\t      Subtotal Price\n------------------------------------------------------------------------------\n";
				for(i=0;find2!=0;find2=find2->aim)
				{
					cout<<setw(4)<<i+1<<setw(17)<<find2->a.code<<setw(17)<<fixed<<setprecision(3)<<find2->a.price<<setw(17)<<setprecision(0)<<find2->a.stock<<"\t      "<<setprecision(3)<<find2->a.subtotal<<endl;
					j+=find2->a.price;
					k+=find2->a.stock;
					l+=find2->a.subtotal;
				}
				cout<<"\n------------------------------------------------------------------------------\nTOTAL:\n"<<setfill(' ')<<setw(11)<<i+1<<" item(s)"<<setfill(' ')<<setw(19)<<fixed<<setprecision(3)<<j<<setfill(' ')<<setw(17)<<setprecision(0)<<k<<"\t      "<<fixed<<setprecision(3)<<l<<"\n------------------------------------------------------------------------------\n";
				for(;;)
				{
					cout<<"Do you want to save? (Yes: Y/y ; No: N/n)\n";
					if(ask())
					{
						i=j=k=l=0;
						long double m=1;
						for(;;)
						{
							show="find "+to_string(m)+".txt";
							ifstream bbb(show.c_str());
							if(bbb.fail()==1)
							{
								bbb.close();
								break;
							}
							bbb.close();
							m++;
						}
						ofstream aaa(show.c_str());
						aaa<<"\n\n------------------------------------------------------------------------------\n No."<<setw(17)<<"Product code"<<setw(17)<<"Unit Price"<<setw(17)<<"Quantity"<<"\t      Subtotal Price\n------------------------------------------------------------------------------\n";
						for(i=0;find1!=0;find1=find1->aim)
						{
							aaa<<setw(4)<<i+1<<setw(17)<<find1->a.code<<setw(17)<<fixed<<setprecision(3)<<find1->a.price<<setw(17)<<setprecision(0)<<find1->a.stock<<"\t      "<<setprecision(3)<<find1->a.subtotal<<endl;
							j+=find1->a.price;
							k+=find1->a.stock;
							l+=find1->a.subtotal;
							i++;
						}
						aaa<<"\n------------------------------------------------------------------------------\nTOTAL:\n"<<setfill(' ')<<setw(11)<<i<<" item(s)"<<setfill(' ')<<setw(19)<<fixed<<setprecision(3)<<j<<setfill(' ')<<setw(17)<<setprecision(0)<<k<<"\t      "<<fixed<<setprecision(3)<<l<<"\n------------------------------------------------------------------------------\n";
						aaa.close();
						cout<<"\nResult had been saved in \""<<show<<"\".\n";
					}
					break;
				}
				break;
			}
			i=0;
			find3=find1=find2;
			while(find3!=0)
			{
				if(strncmp(&x[found],&find3->a.code[found],1)==0)
				{
					if(i==0)
						find1=find2=find3;
					else
					{
						find1->aim=find3;
						find1=find3;
					}
					i++;
				}
				find3=find3->aim;
			}
			find1->aim=0;
		}
		system("pause");
	}
}
void proj::arrange()
{
	char a='6';
	int b;
	for(;;)
	{
		title();
		display_order(a);
		cout<<"\n\nArrangement Type\n\n1 - Product code : XXX00000Y\n2 - Product code : ___00000_\n3 - Product code : ________Y\n4 - Unit Price\n5 - Quantity\n6 - Subtotal\n7 - Save file\n8 - Back\n\t\t\t[NOTE: # Product code - XXX00000Y \; Eg : ABC12345A]";
		checkinput(1,8);
		b=input2;
		if(input2<7)
		{
			cout<<"\n1 - Ascending Order\n2 - Descending Order\n3 - Discard Previous Choice";
			checkinput(1,3);
			switch(input2)
			{
			case 1: a='6';break;
			case 2: a='5';break;
			case 3: continue;
			}
			arranging(9,0);
		}
		switch(b)
		{
		case 1:break;
		case 2:arranging(5,3);break;
		case 3:arranging(1,8);break;
		case 4:arr_num('1');break;
		case 5:arr_num('2');break;
		case 6:arr_num('3');break;
		case 7:
			cout<<"\n\n\nSave current showing arrangement in current file? (Yes: Y/y ; No: N/n)";
			if(ask())
			{
				if(a='6')
					write_file('1');
				else
					write_file('2');
			}
			break;
		case 8:
			dir2=strstr(dir," > Arrange");
			strcpy(dir2,"");
			file();
			break;
		}
	}
}
void proj::compare()
{
	arranging(9,0);
	for(;;)
	{
		int a,b;
		float c,d;
		a=b=c=d=0;
		char e[10];
		string show2;
		title();
		cout<<"\n\nWhich file do u want to compare with current file?\n(Type '?' to return to previous page.)\n\n";
		for(;;)
		{
			cout<<"Filename : ";
			getline(cin,show2);
			if(show2.length()==1&&show2=="?")
			{
				dir2=strstr(dir," > Compare");
				strcpy(dir2,"");
				file();
			}
			show2+=".txt";
			if(show2==filename)
			{
				cout<<"\nCannot compare with the same file.\n";
				system("pause");
				compare();
			}
			ifstream aaa(show2.c_str());
			if(cin.fail()==1)
			{
				cout<<"\nThis file cannot be found.\n";
				system("pause");
				compare();
			}
			else
				break;
			aaa.close();
		}
		ifstream bbb(show2.c_str());
		while(show.length()!=1)
		{
			bbb>>show;
			a++;
			if(a>30)
			{
				cout<<"\nNo data can be found in file within expected range.\n";
				system("pause");
				bbb.close();
				compare();
			}
		}
		if(c<31)
		{
			size_t bb=show.find_first_not_of("1234567890");
			if(bb!=show.npos)
			{
				cout<<"\nThe data cannot be read without following the format.\n";
				system("pause");
			}
			else
			{
				cout<<"(Stop reading file for end of file or irrelevant data found.)\n\n\n\t\t\t\tRESULT\n------------------------------------------------------------------------------\n"<<" Product code"<<setw(17)<<"Unit Price"<<setw(17)<<"Quantity"<<"\t      Subtotal Price\n------------------------------------------------------------------------------\n";
				show="a";
				for(;;)
				{
					bbb>>e;
					if(bbb.fail()||strlen(e)>9)
						break;
					if(!searching(e,0,9))
					{
						cout<<setw(13)<<e<<" (absent)\n";
						show="b";
					}
					bbb>>c;
					if(bbb.fail())
						break;
					bbb>>b;
					if(bbb.fail())
						break;
					bbb>>d;
					if(bbb.fail())
						break;
					if(strcmp(e,find2->a.code)==0)
					{
						cout<<setw(13)<<find2->a.code<<setw(17)<<fixed<<setprecision(3)<<find2->a.price-c<<setw(17)<<setprecision(0)<<find2->a.stock-b<<"\t      "<<setprecision(3)<<find2->a.subtotal-(b*c)<<endl;
					}
					bbb>>a;
					if(bbb.fail())
						break;
				}
				b=c=d=0;
				for(a=0;a<data;a++)
				{
					c+=first->a.price;
					b+=first->a.stock;
					d+=first->a.subtotal;
					first=first->xxx1;
				}
				if(show=="a")
					cout<<"NONE";
				cout<<"\n------------------------------------------------------------------------------\n\nNOTE:\n  1) Result in Difference = \""<<filename<<"\" subtracts \""<<show2<<"\".\n  2) \"absent\" means the corresponding data is not exist in \""<<filename<<"\".\n\n\n\n";
				system("pause");
			}
		}
		bbb.close();
		show.clear();
	}
}
///////////////////////////////////////////////////////////////5th interface////////////////////////////////////////////////////////////////////////////
void proj::add()
{
	for(;;)
	{
		title();
		cout<<"\n\n";
		if(data!=0)
		{
			arranging(9,0);
			display_order('1');
		}
		if(checkinput2('0'))
		{
			dir2=strstr(dir," > Add");
			strcpy(dir2,"");
			building();
		}
		else
			add_data();
	}
}
void proj::update()
{
	for(;;)
	{
		title();
		if(data!=0)
		{
			arranging(9,0);
			display_order('4');
			cout<<"Item to be updated?\n\n";
			if(checkinput2('1'))
			{
				dir2=strstr(dir," > Update");
				strcpy(dir2,"");
				building();
			}
			if(searching(x,0,9))
			{
				cout<<"\n\n------------------------------------------------------------------------------\n Product code"<<setw(17)<<"Unit Price"<<setw(17)<<"Quantity"<<"\t      Subtotal Price\n------------------------------------------------------------------------------\n"<<setw(13)<<find2->a.code<<setw(17)<<fixed<<setprecision(3)<<find2->a.price<<setw(17)<<setprecision(0)<<find2->a.stock<<"\t      "<<setprecision(3)<<find2->a.subtotal<<"\n------------------------------------------------------------------------------\n(Type '?' to cancel the update || 'x' for no update on certain part)\n\n";
				if(!checkinput2('U'))
				{
					strcpy(find2->a.code,x);
					find2->a.price=y;
					find2->a.stock=z;
					find2->a.subtotal=y*z;
					strcpy(curr[2].curr,x);
				}
				else
					update();
			}
			else
				cout<<"This product code cannot be found!\n";
			system("pause");
		}
		else
		{
			cout<<"\n\n\nTotal data = 0\n\n";
			system("pause");
			dir2=strstr(dir," > Update");
			strcpy(dir2,"");
			building();
		}
	}
}
void proj::del()
{
	for(;;)
	{
		title();
		if(data!=0)
		{
			arranging(9,0);
			display_order('3');
			cout<<"Item to be deleted?\n\n";
			if(checkinput2('1'))
			{
				dir2=strstr(dir," > Delete");
				strcpy(dir2,"");
				building();
			}
			if(searching(x,0,9))
			{
				cout<<"\n\n------------------------------------------------------------------------------\n Product code"<<setw(17)<<"Unit Price"<<setw(17)<<"Quantity"<<"\t      Subtotal Price\n------------------------------------------------------------------------------\n"<<setw(13)<<find2->a.code<<setw(17)<<fixed<<setprecision(3)<<find2->a.price<<setw(17)<<setprecision(0)<<find2->a.stock<<"\t      "<<setprecision(3)<<find2->a.subtotal<<"\n------------------------------------------------------------------------------\nAre you sure? (Yes: Y/y ; No: N/n)";
				if(!ask())
					del();
				strcpy(curr[1].curr,x);
				find1=find2->xxx2;
				find3=find2->xxx1;
				if(find2==first)
					first=find3;
				store_undo();
				delete find2;
				find1->xxx1=find3;
				find3->xxx2=find1;
				last=first->xxx2;
				data--;
				cout<<curr[1].curr<<" had been deleted.\n\n";
			}
			else
				cout<<"This product code cannot be found!\n\n";
			system("pause");
		}
		else
		{
			cout<<"\n\n\nTotal data = 0\n\n";
			system("pause");
			dir2=strstr(dir," > Delete");
			strcpy(dir2,"");
			building();
		}
	}
}
void proj::del_history()
{
	for(;;)
	{
		title();
		if(count!=0)
		{
			cout<<"\n\nData Recovery List:\n\n\n------------------------------------------------------------------------------\n No."<<setw(17)<<"Product code"<<setw(17)<<"Unit Price"<<setw(17)<<"Quantity"<<"\t      Subtotal Price\n------------------------------------------------------------------------------\n[Newest]\n\n";
			for(int i=count;i>0;i--)
				cout<<setw(4)<<i<<setw(17)<<backup[i-1].code<<setw(17)<<fixed<<setprecision(3)<<backup[i-1].price<<setw(17)<<setprecision(0)<<backup[i-1].stock<<"\t      "<<setprecision(3)<<backup[i-1].subtotal<<endl;
			cout<<"\n[Oldest]\n------------------------------------------------------------------------------\n\n\nEnter "<<count+1<<" to return. (Input range: 1 - "<<count+1<<")\n\n";
			checkinput(1,count+1);
			if(input2==count+1)
			{
				dir2=strstr(dir," > Data Recovery");
				strcpy(dir2,"");
				building();
			}
			else
			{
				cout<<"Recover? (Yes: Y/y ; No: N/n)";
				if(!ask())
					del_history();
				strcpy(x,backup[input2-1].code);
				y=backup[input2-1].price;
				z=backup[input2-1].stock;
				add_data();
				count--;
				for(int i=input2-1;i<count;i++)
				{
					strcpy(backup[i].code,backup[i+1].code);
					backup[i].price=backup[i+1].price;
					backup[i].stock=backup[i+1].stock;
					backup[i].subtotal=backup[i+1].subtotal;
				}
			}
		}
		else
		{
			cout<<"\n\n\nNo recovery data.\n\n";
			system("pause");
			dir2=strstr(dir," > Data Recovery");
			strcpy(dir2,"");
			building();
		}
			
	}
}
void proj::saving()
{
	title();
	if(data==0)
	{
		cout<<"\n\n\nTotal data found : 0\nYou cannot save with zero data.\n\n\n";
	}
	else
	{
		cout<<"\n\nDo you want to save your data?\n\n - If yes, all your previous data will be overwritten without warning.\n - If no, all the updated data or changing will be lost.\n\n[Yes: Y/y \; No: N/n]\n\n";
		if(ask())
		{
			write_file('1');
			if(build_cond=='F')
			{
				dir2=strstr(dir," > New > Build > Save");
				strcpy(dir2,"");
				strcat(dir," > Open > File > Edit > Save");
			}
			build_cond='P';
			cout<<"\nData saved. ";
		}
		else
			cout<<"\nData not saved. ";
	}
	system("pause");
	dir2=strstr(dir," > Save");
	strcpy(dir2,"");
	building();
}